//
//  ViewController.swift
//  Aktuel Market indirimler
//
//  Created by Atakan Cengiz KURT on 7.08.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import Firebase
import Crashlytics


class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var collectionView: UICollectionView!
    
   
    
    let service: MarketCampaignsServiceProtocol = MarketCampaignsService()
    

    var models = [Market]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        service.fetchMarketCampaigns { (result) in
            print(result)
            
        }
    
        
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
//        return Array(self.value).count
   
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let detailViewController: DetailViewController = PDVAPI_PageManager.getStoryboardWithIdentifierWithStoryboardId("Main", "detailViewController") as? DetailViewController {
//            detailViewController.jsonObject = jsonValues[indexPath.row]
            
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if let cell:CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as? CollectionViewCell{
            
//            let url = URL(string: "url_of_your_image")
//            imageView.kf.setImage(with: url)
            
//            cell.image.image = UIImage(named: self.images[indexPath.row])
            
//            cell.reloadData(JSON(models[indexPath.row]))
//            cell.lblTitle.text = models[indexPath.row].wallTags
//            cell.lblDate.text = models[indexPath.row].categoryName
            
            
            return cell
            
        }else{
            return CollectionViewCell()
        }
        
    }
    
    
    
}

