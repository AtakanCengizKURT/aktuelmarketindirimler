//
//  Market.swift
//  Aktuel Market indirimler
//
//  Created by Atakan Cengiz KURT on 24.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import Foundation


public struct Market: Decodable {
    public enum CodingKeys: String, CodingKey{
        case cid 
        case wallpaperImage = "wallpaper_image"
        case categoryName = "category_name"
        case categoryImage = "category_image"
        case wallTags = "wall_tags"
        
    }
    public let cid: String
    public let wallpaperImage: URL
    public let categoryName: String
    public let categoryImage: URL
    public let wallTags: String
    
}
