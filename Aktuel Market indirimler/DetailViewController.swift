//
//  DetailViewController.swift
//  Aktuel Market indirimler
//
//  Created by Atakan Cengiz KURT on 20.08.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import SwiftyJSON

class DetailViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var pageControl : UIPageControl!
    @IBOutlet weak var scrollView : UIScrollView!
    
    var jsonObject : JSON?
    var images : JSON = JSON()
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let jsonObject = jsonObject{
            self.images = jsonObject["pages"]
            print("images**\(jsonObject)")
        }
        
        pageControl.numberOfPages = (images.count)
        for index in 0..<(images.count){
            frame.origin.x = scrollView.frame.size.width * CGFloat(index)
            frame.size = scrollView.frame.size
            let imgView = UIImageView(frame: frame)
//            imgView.image = UIImage(named: images[index])
            imgView.kf.setImage(with: URL(string: "\(images[index].stringValue)"))
            self.scrollView.addSubview(imgView)
        }
        
        scrollView.contentSize = CGSize(width: (scrollView.frame.size.width * CGFloat((images.count))), height: scrollView.frame.size.height)
        scrollView.delegate = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.currentPage = Int(pageNumber)
        
        
    }

}
