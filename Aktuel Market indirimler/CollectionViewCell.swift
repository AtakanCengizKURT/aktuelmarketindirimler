//
//  CollectionViewCell.swift
//  Aktuel Market indirimler
//
//  Created by Atakan Cengiz KURT on 10.08.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import SwiftyJSON

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    private var jsonObject: JSON?
    
    func reloadData(_ json: JSON){
        self.jsonObject = json
        self.image.kf.setImage(with: URL(string: "\(json["pages"][0].stringValue)"))
    }
}
