//
//  Result.swift
//  Aktuel Market
//
//  Created by Atakan Cengiz KURT on 24.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import Foundation

public enum Result<Value>{
    case success(Value)
    case failure(Error)
}
