//
//  MarketCampaignsService.swift
//  Aktuel Market
//
//  Created by Atakan Cengiz KURT on 24.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import Foundation
import Alamofire


public protocol MarketCampaignsServiceProtocol {
    func fetchMarketCampaigns(completion: @escaping (Result<MarketCampaignsResponse>)-> Void)
}


public class MarketCampaignsService:MarketCampaignsServiceProtocol{
   public enum Error: Swift.Error{
        case serializationError(internal: Swift.Error)
        case networkError(internal: Swift.Error)
    }
    
    public init(){ }
    
   public func fetchMarketCampaigns(completion: @escaping ((Result<MarketCampaignsResponse>) -> Void)) {
        let urlString = "http://aktuelv2.haftaninurunleri.com/api.php?home"
    
    request(urlString).responseData { (response) in
    
        switch response.result{
            case .success(let data):
                let decoder = Decoders.decoder
                do{
                    let response = try decoder.decode(MarketCampaignsResponse.self, from: data)
                    completion(.success(response))
                }catch{
                    completion(.failure(Error.serializationError(internal: error)))
                }
                
                
            case .failure(let error):
                completion(.failure(Error.networkError(internal: error)))
            }
        }
    }
    
  
}
