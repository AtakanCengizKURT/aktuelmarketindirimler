//
//  MartketCampaigns.swift
//  Aktuel Market
//
//  Created by Atakan Cengiz KURT on 24.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import Foundation

public struct MarketCampaignsResponse: Decodable{
    
    private enum RootCodingKeys: String, CodingKey{
        case HD_WALLPAPER
    }
    
    private enum HD_WALLPAPERCodingKeys: String, CodingKey{
    case latest_wallpaper
    }
    
    
   public let results: [Market]
    
    init(results: [Market]) {
        self.results = results
    }
    
   public init(from decoder: Decoder) throws{
        let rootContainer = try decoder.container(keyedBy: RootCodingKeys.self)
        let HD_WALLPAPERContainer = try rootContainer.nestedContainer(keyedBy: HD_WALLPAPERCodingKeys.self, forKey: .HD_WALLPAPER)
        self.results = try HD_WALLPAPERContainer.decode([Market].self, forKey: .latest_wallpaper)
    }
}
